package com.suleymanbilgin.myonlinebasket;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.suleymanbilgin.myonlinebasket.custom.BaseActivity;
import com.suleymanbilgin.myonlinebasket.utils.AppValues;
import com.suleymanbilgin.myonlinebasket.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * E-mail ile giriş yapılan ekran
 * <p/>
 * Created by Laptop on 19/04/2016.
 */
public class LoginActivity extends AppCompatActivity {

    RequestQueue mRequestQueue;
    CardView cv_login;
    EditText et_mail, et_password;
    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();


    }

    void init() {
        cv_login = (CardView) findViewById(R.id.cv_login);
        et_mail = (EditText) findViewById(R.id.et_mail);
        et_password = (EditText) findViewById(R.id.et_password);

        mRequestQueue = Volley.newRequestQueue(this);

        sharedPref = getSharedPreferences("com.suleymanbilgin.myonlinebasket.prefs", Context.MODE_PRIVATE);

        cv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JsonArrayRequest json = new JsonArrayRequest(Request.Method.GET,
                        AppValues.LINK.LOGIN + "mail=" + et_mail.getText().toString() + "&password=" + Utils.md5(et_password.getText().toString())
                        , null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        if (response.length() != 0) {
                            JSONObject temp = null;
                            int ID = 0;
                            String name = "", surname = "", password = "", mail = "", image = "";

                            // for her zaman 1 kere döner fakat geriye dönen değer array olduğu için böyle yapıyorum
                            for (int i = 0; i < response.length(); i++) {
                                try {
                                    temp = response.getJSONObject(i);
                                    ID = temp.getInt("id");
                                    name = temp.getString("name");
                                    surname = temp.getString("surname");
                                    mail = temp.getString("mail");
                                    image = temp.getString("image");
                                    password = temp.getString("password");
                                } catch (Exception e) {
                                }
                            }


                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString(AppValues.USER.NAME, name);
                            editor.putString(AppValues.USER.SURNAME, surname);
                            editor.putString(AppValues.USER.PASSWORD, password);
                            editor.putString(AppValues.USER.MAIL, mail);
                            editor.putString(AppValues.USER.IMAGE, image);
                            editor.putInt(AppValues.USER.USER_ID, ID);
                            editor.commit();
                            Intent main = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(main);
                            finish();
                        } else {
                            try {
                                Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.not_found_user), Snackbar.LENGTH_LONG)
                                        .show();
                            } catch (Exception e) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.not_found_user), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("e", "Not Logged In " + error.toString());
                    }
                });
                mRequestQueue.add(json);
            }
        });
    }
}
