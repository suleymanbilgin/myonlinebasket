package com.suleymanbilgin.myonlinebasket;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.suleymanbilgin.myonlinebasket.custom.BaseActivity;
import com.suleymanbilgin.myonlinebasket.utils.AppValues;
import com.suleymanbilgin.myonlinebasket.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Laptop on 19/04/2016.
 */
public class CreateAccountActivity extends BaseActivity {
    Toolbar toolbar;
    EditText etMail, etPassword, etConfirmPassword, etNameSurname;
    CardView cvCreateAccount;
    int i;
    String name = "", surname = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        init();
    }

    void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.create_account));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        mRequestQueue = Volley.newRequestQueue(this);

        etMail = (EditText) findViewById(R.id.et_mail);
        etPassword = (EditText) findViewById(R.id.et_password);
        etConfirmPassword = (EditText) findViewById(R.id.et_confirm_password);
        etNameSurname = (EditText) findViewById(R.id.et_name_surname);
        cvCreateAccount = (CardView) findViewById(R.id.cv_create_account);

        cvCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userRegister();
            }
        });
    }

    void userRegister() {
        String Main_String = etNameSurname.getText().toString();
        String[] nameArray = Main_String.split(" ");
        for (i = 0; i < nameArray.length; i++) {
            if (i != (nameArray.length - 1))
                name = name + nameArray[i] + "-";
            else
                surname = nameArray[i];
        }


        if (etPassword.length() < 4) {
            try {
                Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.password_length), Snackbar.LENGTH_LONG)
                        .show();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.password_length), Toast.LENGTH_LONG).show();
            }
        } else {
            String url = AppValues.LINK.REGISTER
                    + "?name=" + name
                    + "&surname=" + surname
                    + "&mail=" + etMail.getText().toString()
                    + "&password=" + Utils.md5(etPassword.getText().toString());
            JsonObjectRequest json = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("NULL", String.valueOf(response));
                    if (response.length() != 0) {
                        JSONObject temp = null;
                        int ID = 0;
                        String name = "", surname = "", password = "", mail = "";

                        if (response.length() == 0) {
                            try {
                                Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.register_failed), Snackbar.LENGTH_LONG)
                                        .show();
                            } catch (Exception e) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.register_failed), Toast.LENGTH_LONG).show();
                            }
                        } else {
                            try {
                                ID = response.getInt("id");
                                name = response.getString("name");
                                surname = response.getString("surname");
                                mail = response.getString("mail");
                                password = response.getString("password");


                                Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.register_done), Snackbar.LENGTH_LONG)
                                        .show();
                            } catch (Exception e) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.register_done), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Error", String.valueOf(error));
                    try {
                        Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.error), Snackbar.LENGTH_LONG)
                                .show();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.error), Toast.LENGTH_LONG).show();
                    }
                }
            });
            mRequestQueue.add(json);
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    //Menu items onClick
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
