package com.suleymanbilgin.myonlinebasket;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.suleymanbilgin.myonlinebasket.adapter.BasketDetailAdapter;
import com.suleymanbilgin.myonlinebasket.adapter.BasketListAdapter;
import com.suleymanbilgin.myonlinebasket.custom.BaseActivity;
import com.suleymanbilgin.myonlinebasket.models.Basket;
import com.suleymanbilgin.myonlinebasket.models.BasketProduct;
import com.suleymanbilgin.myonlinebasket.utils.AppValues;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by suleyman on 26.04.2016.
 */
public class BasketDetailActivity extends BaseActivity {
    CheckBox cbProductName;
    TextView tvProductCount;
    List<BasketProduct> mBasketProductList;
    int basket_id;
    RecyclerView rvBasketProductList;
    Context mContext;
    BasketDetailActivity bdAct;
    LinearLayoutManager mLinearLayoutManager;
    BasketDetailAdapter mBasketDetailAdapter;
    SharedPreferences sharedPref;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basket_detail);

        init();
    }

    void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.basket_details));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        Bundle extras = getIntent().getExtras();
        basket_id = extras.getInt("basket_id");
        sharedPref = getSharedPreferences("com.suleymanbilgin.myonlinebasket.prefs", Context.MODE_PRIVATE);

        mRequestQueue = Volley.newRequestQueue(this);

        rvBasketProductList = (RecyclerView) findViewById(R.id.rv_basket_product_list);
        mContext = getApplicationContext();
        bdAct = (BasketDetailActivity) BasketDetailActivity.this;
        mLinearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);

        getBasketProducts();
    }

    void getBasketProducts() {
        String url = AppValues.LINK.SHOW_BASKET_PRODUCTS + "/" + String.valueOf(basket_id) + ".json";
        final int user_id = sharedPref.getInt(AppValues.USER.USER_ID, 0);

        JsonObjectRequest json = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray basketProducts = response.getJSONArray("basketprodcuts");
                    mBasketProductList = new ArrayList<BasketProduct>();

                    for (int i = 0; i < basketProducts.length(); i++) {
                        JSONObject product = basketProducts.getJSONObject(i);
                        BasketProduct tmpBasketProduct = new BasketProduct();
                        tmpBasketProduct.setId(product.getInt("id"));
                        tmpBasketProduct.setUser_id(product.getInt("user_id"));
                        tmpBasketProduct.setProductName(product.getString("productname"));
                        tmpBasketProduct.setCount(product.getString("count"));
                        tmpBasketProduct.setCountable(product.getString("countable"));
                        tmpBasketProduct.setBought(product.getString("bought"));
                        tmpBasketProduct.setBasket_id(product.getInt("mbasket_id"));

                        mBasketProductList.add(tmpBasketProduct);
                    }

                    mBasketDetailAdapter = new BasketDetailAdapter(mBasketProductList, mContext, bdAct, mRequestQueue, user_id);
                    rvBasketProductList.setHasFixedSize(true);
                    rvBasketProductList.setLayoutManager(mLinearLayoutManager);

                    //Set the empty view
                    if (rvBasketProductList != null) {
                        rvBasketProductList.setAdapter(mBasketDetailAdapter);
                        Log.e("i", String.valueOf("not null"));
                    }

                    try {
                        Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.listed_all_items), Snackbar.LENGTH_LONG)
                                .show();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.listed_all_items), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    Log.e("Error", String.valueOf(e));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        mRequestQueue.add(json);
    }

    @Override
    protected void onResume() {
        super.onResume();

        getBasketProducts();
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.basket_detail, menu);
        return true;
    }

    //Menu items onClick
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            case R.id.action_add_new_product:
                Intent addProductActivity = new Intent(this, AddNewProductToBasketActivity.class);
                addProductActivity.putExtra("basket_id", basket_id);
                startActivity(addProductActivity);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
