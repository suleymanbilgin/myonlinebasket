package com.suleymanbilgin.myonlinebasket;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.suleymanbilgin.myonlinebasket.custom.BaseActivity;
import com.suleymanbilgin.myonlinebasket.utils.AppValues;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Laptop on 22/04/2016.
 */
public class CreateNewBasketActivity extends BaseActivity {
    LinearLayout llBelow;
    CheckBox cbItems;
    Toolbar toolbar;
    CardView cvCreateNewBasket;
    EditText etNewBasketName;
    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_basket);

        init();
    }

    void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.create_new_basket));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mRequestQueue = Volley.newRequestQueue(this);

        llBelow = (LinearLayout) findViewById(R.id.ll_below);
        cbItems = (CheckBox) findViewById(R.id.cb_below);
        cbItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cbItems.isChecked()) {
                    llBelow.setVisibility(View.VISIBLE);
                } else {
                    llBelow.setVisibility(View.GONE);
                }
            }
        });

        etNewBasketName = (EditText) findViewById(R.id.et_new_basket_name);
        cvCreateNewBasket = (CardView) findViewById(R.id.cv_create_new_basket);
        sharedPref = getSharedPreferences("com.suleymanbilgin.myonlinebasket.prefs", Context.MODE_PRIVATE);
        final int ID = sharedPref.getInt(AppValues.USER.USER_ID, 0);

        cvCreateNewBasket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewBasket(etNewBasketName.getText().toString(), ID);
            }
        });
    }

    void createNewBasket(String newBasketName, int userId) {
        String url = AppValues.LINK.CREATE_NEW_BASkET
                + "?basketname=" + newBasketName
                + "&user_id=" + String.valueOf(userId);
        url = url.replaceAll(" ", "%20");

        // Log.e("url", url);
        // http://198.199.107.154/baskets.json?basketname=Ev&user_id=1

        JsonObjectRequest json = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject basket = response.getJSONObject("basket");
                    JSONObject creator = response.getJSONObject("creator");

                    try {
                        Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.success_new_basket), Snackbar.LENGTH_LONG)
                                .show();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.success_new_basket), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        mRequestQueue.add(json);
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    //Menu items onClick
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
