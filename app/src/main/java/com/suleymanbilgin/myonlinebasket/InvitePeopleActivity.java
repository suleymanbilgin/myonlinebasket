package com.suleymanbilgin.myonlinebasket;

import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.suleymanbilgin.myonlinebasket.custom.BaseActivity;
import com.suleymanbilgin.myonlinebasket.utils.Utils;

/**
 * Created by Laptop on 21/04/2016.
 */
public class InvitePeopleActivity extends AppCompatActivity {
    Toolbar toolbar;
    CheckBox checkBox;
    LinearLayout llCheckbox, llMail;
    ImageView ivPlus;
    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_people);

        init();

    }

    void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.invite_people));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        llCheckbox = (LinearLayout) findViewById(R.id.ll_checkbox);
        llMail = (LinearLayout) findViewById(R.id.ll_mail);

        ivPlus = (ImageView) findViewById(R.id.iv_plus);
        ivPlus.setId(++i);
        final LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        ivPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextInputLayout til = new TextInputLayout(InvitePeopleActivity.this);
                til.setLayoutParams(lp);
                llMail.addView(til);

                EditText etMail = new EditText(InvitePeopleActivity.this);
                etMail.setId(++i);
                etMail.setLayoutParams(lp);
                etMail.setHintTextColor(Utils.getColor(getApplicationContext(), R.color.text_gray_login));
                etMail.setHint(getString(R.string.mail));
                til.addView(etMail);

                /*
                final CheckBox checkBoxa = new CheckBox(InvitePeopleActivity.this);
                checkBoxa.setId(++i);
                checkBoxa.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.e("e", String.valueOf(checkBoxa.getId()) + " numaralı checkbox seçildi");
                    }
                });
                checkBoxa.setText("I'm dynamic!");
                llCheckbox.addView(checkBoxa);*/
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    //Menu items onClick
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
