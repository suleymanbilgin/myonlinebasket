package com.suleymanbilgin.myonlinebasket;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;

import com.suleymanbilgin.myonlinebasket.custom.BaseActivity;

/**
 * Created by Laptop on 18/04/2016.
 */
public class LoginSelectionActivity extends AppCompatActivity {

    CardView cvLogin, cvSignInWithGoogle, cvSignInWithMail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_selection);

        init();


    }

    void init() {
        cvLogin = (CardView) findViewById(R.id.cv_login);
        cvSignInWithGoogle = (CardView) findViewById(R.id.cv_login_with_google);
        cvSignInWithMail = (CardView) findViewById(R.id.cv_login_with_mail);

        cvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        cvSignInWithGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signInWithGoogle();
            }
        });

        cvSignInWithMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signInWithMail();
            }
        });
    }

    void login() {
        Intent intent = new Intent(LoginSelectionActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    void signInWithGoogle() {

    }

    void signInWithMail() {
        Intent intent = new Intent(LoginSelectionActivity.this, CreateAccountActivity.class);
        startActivity(intent);
    }
}
