package com.suleymanbilgin.myonlinebasket;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.suleymanbilgin.myonlinebasket.custom.BaseActivity;
import com.suleymanbilgin.myonlinebasket.settings.GeneralFragment;

/**
 * This class contains application settings .
 *
 * @author Promob Web and App Development
 * @since 1.0
 */
public class SettingsActivity extends BaseActivity {

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_settings);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(getString(R.string.settings));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String action = getIntent().getAction();
        if (action != null && action.equals("pref_dis_general")) {

            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_frame, new GeneralFragment())
                    .commit();
        } else if (action != null && action.equals("pref_dis_notification")) {
           /* getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_frame, new NotificationFragment())
                    .commit();*/
        } else if (action != null && action.equals("pref_dis_info")) {
           /* getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_frame, new InfoFragment())
                    .commit();*/
        } else if (action != null && action.equals("pref_dis_links")) {
            /*getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_frame, new LinksFragment())
                    .commit();*/
        } else {
            /*getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_frame, new StartFragment())
                    .commit();*/
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
