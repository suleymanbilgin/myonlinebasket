package com.suleymanbilgin.myonlinebasket.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.jorgecastilloprz.FABProgressCircle;
import com.suleymanbilgin.myonlinebasket.R;
import com.suleymanbilgin.myonlinebasket.custom.BaseFragment;

/**
 * Created by Laptop on 22/04/2016.
 */
public class ProfileFragment extends BaseFragment {
    View view;
    FloatingActionButton fabView;
    FABProgressCircle fabProgressCircle;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);

        init();

        return view;
    }

    void init() {
        fabView = (FloatingActionButton) view.findViewById(R.id.fab);
        fabProgressCircle = (FABProgressCircle) view.findViewById(R.id.fabProgressCircle);

        fabView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabProgressCircle.show();
                // startYourAsynchronousJob();
            }
        });


    }
}
