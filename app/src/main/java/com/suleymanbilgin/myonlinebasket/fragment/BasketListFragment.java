package com.suleymanbilgin.myonlinebasket.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.clans.fab.FloatingActionButton;
import com.suleymanbilgin.myonlinebasket.CreateNewBasketActivity;
import com.suleymanbilgin.myonlinebasket.InvitePeopleActivity;
import com.suleymanbilgin.myonlinebasket.MainActivity;
import com.suleymanbilgin.myonlinebasket.R;
import com.suleymanbilgin.myonlinebasket.adapter.BasketListAdapter;
import com.suleymanbilgin.myonlinebasket.custom.BaseFragment;
import com.suleymanbilgin.myonlinebasket.models.Basket;
import com.suleymanbilgin.myonlinebasket.utils.AppValues;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Laptop on 20/04/2016.
 */
public class BasketListFragment extends BaseFragment {
    View view;
    FloatingActionButton fabInvitePeople, fabCreateNewBasket;
    List<Basket> basketsList;
    SharedPreferences sharedPref;
    BasketListAdapter mBasketListAdapter;
    RecyclerView rvBasketList;
    Context mContext;
    MainActivity mAct;
    LinearLayoutManager mLinearLayoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_basket_list, container, false);

        init();

        return view;
    }

    void init() {
        sharedPref = getActivity().getSharedPreferences("com.suleymanbilgin.myonlinebasket.prefs", Context.MODE_PRIVATE);

        mRequestQueue = Volley.newRequestQueue(getActivity());

        fabInvitePeople = (FloatingActionButton) view.findViewById(R.id.fab_invite_people);
        fabInvitePeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), InvitePeopleActivity.class);
                startActivity(intent);
            }
        });
        fabCreateNewBasket = (FloatingActionButton) view.findViewById(R.id.fab_create_new_basket);
        fabCreateNewBasket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CreateNewBasketActivity.class);
                startActivity(intent);
            }
        });

        rvBasketList = (RecyclerView) view.findViewById(R.id.rv_basket_list);
        mContext = getActivity().getApplicationContext();
        mAct = (MainActivity) getActivity();
        mLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        getBasketList();
    }

    void getBasketList() {
        int user_id = sharedPref.getInt(AppValues.USER.USER_ID, 0);

        if (user_id != 0) {
            String url = AppValues.LINK.SHOW_USER_BASKETS + "/" + String.valueOf(user_id) + "/showbaskets.json";
            Log.i("url", url);
            JsonObjectRequest json = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONArray basketList = response.getJSONArray("baskets");
                        Log.i("i", String.valueOf(basketList.length()));

                        basketsList = new ArrayList<Basket>();

                        for (int i = 0; i < basketList.length(); i++) {
                            JSONObject tmpObjectBasket = basketList.getJSONObject(i);
                            Log.i("tmpObjectBasket", String.valueOf(tmpObjectBasket));

                            Basket tmpBasket = new Basket();

                            tmpBasket.setId(tmpObjectBasket.getInt("id"));
                            tmpBasket.setUser_id(tmpObjectBasket.getInt("user_id"));
                            tmpBasket.setBasketname(tmpObjectBasket.getString("basketname"));
                            basketsList.add(tmpBasket);
                            Log.i("i", String.valueOf(tmpBasket.getId()));

                        }

                        mBasketListAdapter = new BasketListAdapter(basketsList, mContext, mAct);
                        rvBasketList.setHasFixedSize(true);
                        rvBasketList.setLayoutManager(mLinearLayoutManager);

                        //Set the empty view
                        if (rvBasketList != null) {
                            rvBasketList.setAdapter(mBasketListAdapter);
                            Log.e("i", String.valueOf("not null"));
                        }

                        Log.i("response", String.valueOf(response));

                    } catch (Exception e) {
                        Log.i("Exception", String.valueOf(e));

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("error", String.valueOf(error));
                }
            });
            mRequestQueue.add(json);
        } else {
            try {
                Snackbar.make(getActivity().findViewById(android.R.id.content), getResources().getString(R.string.not_found_user), Snackbar.LENGTH_LONG)
                        .show();
            } catch (Exception e) {
                Toast.makeText(getActivity(), getResources().getString(R.string.not_found_user), Toast.LENGTH_LONG).show();
            }
        }
/*
        Gson gson = new Gson();
        recentposts = gson.fromJson(string_json, AllPosts.class);
        total_pages = recentposts.getPages();*/
    }
}
