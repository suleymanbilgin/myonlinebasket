package com.suleymanbilgin.myonlinebasket.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.suleymanbilgin.myonlinebasket.R;
import com.suleymanbilgin.myonlinebasket.custom.BaseFragment;

/**
 * Created by Laptop on 22/04/2016.
 */
public class AboutFragment extends BaseFragment {
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_about, container, false);

        return view;
    }
}
