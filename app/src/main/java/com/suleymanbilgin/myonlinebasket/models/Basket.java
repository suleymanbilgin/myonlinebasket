package com.suleymanbilgin.myonlinebasket.models;

/**
 * Created by suleyman on 25.04.2016.
 */
public class Basket {
    private int id;
    private int user_id;
    private String basketname;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getBasketname() {
        return basketname;
    }

    public void setBasketname(String basketname) {
        this.basketname = basketname;
    }
}
