package com.suleymanbilgin.myonlinebasket.models;

/**
 * Created by suleyman on 26.04.2016.
 */
public class BasketProduct {
    int id;
    String productName;
    String countable;
    String count;
    int user_id;
    String bought;
    int basket_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCountable() {
        return countable;
    }

    public void setCountable(String countable) {
        this.countable = countable;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getBought() {
        return bought;
    }

    public void setBought(String bought) {
        this.bought = bought;
    }

    public int getBasket_id() {
        return basket_id;
    }

    public void setBasket_id(int basket_id) {
        this.basket_id = basket_id;
    }


}
