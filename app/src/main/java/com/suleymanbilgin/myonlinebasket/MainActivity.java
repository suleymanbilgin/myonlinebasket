package com.suleymanbilgin.myonlinebasket;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.suleymanbilgin.myonlinebasket.custom.BaseActivity;
import com.suleymanbilgin.myonlinebasket.fragment.AboutFragment;
import com.suleymanbilgin.myonlinebasket.fragment.BasketListFragment;
import com.suleymanbilgin.myonlinebasket.fragment.FirstFragment;
import com.suleymanbilgin.myonlinebasket.fragment.ProfileFragment;
import com.suleymanbilgin.myonlinebasket.fragment.SecondFragment;
import com.suleymanbilgin.myonlinebasket.fragment.ThreeFragment;
import com.suleymanbilgin.myonlinebasket.utils.AppValues;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends BaseActivity implements
        NavigationView.OnNavigationItemSelectedListener {

    private static final long DRAWER_CLOSE_DELAY_MS = 250;
    private static final String NAV_ITEM_ID = "navItemId";

    private final Handler mDrawerActionHandler = new Handler();
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private int mNavItemId;

    Toolbar toolbar;

    CircleImageView profileImage;
    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

        // İlk başta hangi fragment açılacaksa o belirleniyor.
        if (null == savedInstanceState) {
            mNavItemId = R.id.basket; //Drawer'daki id'ye göre
        } else {
            mNavItemId = savedInstanceState.getInt(NAV_ITEM_ID);
        }
        drawerLayoutSetup();
    }

    void init() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.app_name));
    }

    /**
     * Bu metod navigation drawerı oluşturuyor
     */
    public void drawerLayoutSetup() {
        // Navigasyon olaylarını dinler
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation);
        View headerView = navigationView.inflateHeaderView(R.layout.drawer_header);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        profileImage = (CircleImageView) headerView.findViewById(R.id.profile_image);
        TextView tvName = (TextView) headerView.findViewById(R.id.tv_name);
        try {
            sharedPref = getSharedPreferences("com.suleymanbilgin.myonlinebasket.prefs", Context.MODE_PRIVATE);
            String image = sharedPref.getString(AppValues.USER.IMAGE, "");
            String name = sharedPref.getString(AppValues.USER.NAME, "") + " " + sharedPref.getString(AppValues.USER.SURNAME, "");
            tvName.setText(name);
            Picasso.with(getApplicationContext()).load(image).into((CircleImageView) profileImage);
        } catch (Exception e) {
        }

        // navigasyon elemanı seçimi
        try {
            navigationView.getMenu().findItem(mNavItemId).setChecked(true);
        } catch (Exception e) {
        }

        // hamburger ikonu aç kapa
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open,
                R.string.close);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        navigate(mNavItemId);
    }

    /**
     * Bu metod fragmenti activiteye tutturur. İsteklerinize göre özelleştirebilirsiniz.
     *
     * @param itemId navigasyon drawerdaki itemın id'si
     */
    private void navigate(final int itemId) {
        Fragment f = null;
        switch (itemId) {
            case R.id.basket:
                f = new BasketListFragment();
                launchFragment(f, R.string.homepage);
                getSupportActionBar().setTitle(getString(R.string.app_name));
                break;
            case R.id.create_new_basket:
                Intent newBasketIntent = new Intent(MainActivity.this, CreateNewBasketActivity.class);
                startActivity(newBasketIntent);
                break;
            case R.id.profile:
                f = new ProfileFragment();
                launchFragment(f, R.string.profile_settings);
                getSupportActionBar().setTitle(getString(R.string.profile_settings));
                break;
            case R.id.send_invites:
                Intent sendIntent = new Intent(MainActivity.this, InvitePeopleActivity.class);
                startActivity(sendIntent);
                break;
            case R.id.settings:
                Intent settingsIntent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(settingsIntent);
                break;
            case R.id.about:
                f = new AboutFragment();
                launchFragment(f, R.string.about);
                getSupportActionBar().setTitle(getString(R.string.about));
                break;
            case R.id.logout:
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.clear().apply();
                finish();
                break;
            default:
                break;
        }
    }

    /**
     * Fragmenti çalıştırır
     *
     * @param f        Seçilen fragmenti alır
     * @param title_id string id'si alır
     */
    public void launchFragment(Fragment f, int title_id) {
        if (f != null) {
            while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStackImmediate();
            }
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frame, f).addToBackStack(getResources().getString(title_id))
                    .commit();
        }
    }

    @Override
    public boolean onNavigationItemSelected(final MenuItem menuItem) {
        // update highlighted item in the navigation menu
        menuItem.setChecked(true);
        mNavItemId = menuItem.getItemId();

        // allow some time after closing the drawer before performing real navigation
        // so the user can see what is happening
        mDrawerLayout.closeDrawer(GravityCompat.START);
        mDrawerActionHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                navigate(menuItem.getItemId());
            }
        }, DRAWER_CLOSE_DELAY_MS);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }
}
