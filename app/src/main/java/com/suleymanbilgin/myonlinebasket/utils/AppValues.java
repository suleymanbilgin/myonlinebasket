package com.suleymanbilgin.myonlinebasket.utils;

/**
 * SharedPreference özelliklerinin kullanılmasını sağlamaktadır.
 *
 * Created by Laptop on 19/04/2016.
 */
public class AppValues {

    /**
     * Kullanıcı bilgilerini tutmak için kullanılacaktır.
     * */
    public static class USER {
        public boolean LOGGED_IN = false;

        public static String NAME = "name";
        public static String SURNAME = "surname";
        public static String PASSWORD = "password";
        public static String USER_ID = "user_id";
        public static String MAIL = "mail";
        public static String IMAGE = "image";
        public static String DEVICE = "device";
        public static String GCM = "gcm";
    }

    public static class BASKET {
        public static String BASKET_NAME = "basketname";
        public static String USER_ID = "user_id";
    }

    public static class BASKET_PRODUCTS {
        public static String PRODUCT_NAME = "productname";
        public static String COUNTABLE = "countable";
        public static String COUNT = "count";
        public static String USER_ID = "user_id";
        public static String BOUGHT = "bought";
        public static String MBASKET_ID = "mbasket_id";
    }

    /**
     * Uygulama içinde kullanılacak linkler
     * */
    public static class LINK {
        public static String link = "http://198.199.107.154/";

        public static String user = "users";
        public static String LOGIN = link + user + "/show.json?";          // (GET)    mail=slymnbilgin@gmail.com&password=123456
        public static String SHOW_USER_INFORMATIONS = link + user ;        // (GET)    1.json
        public static String UPDATE_USER = link + user;                    // (PATCH)  3.json?name=ahmet123
        public static String REGISTER = link + user + ".json";             // (POST)   name, surname, password, mail
        public static String SHOW_USER_BASKETS = link + user;              // (POST)   name, surname, password, mail

        public static String basket = "baskets";
        public static String CREATE_NEW_BASkET = link + basket + ".json";
        public static String SHOW_BASKET_INFORMATIONS = link + basket;

        public static String basket_product = "basket_products";
        public static String CREATE_NEW_BASKET_PRODUCT = link + basket_product + ".json";   // post --> productname, countable, count, user_id, bought, mbasket_id
        public static String SHOW_BASKET_PRODUCTS = link + basket_product;                  // get  --> basket_id si id olarak gönderilecek
        public static String UPDATE_BASKET_PRODUCTS = link + basket_product;                  // get  --> basket_id si id olarak gönderilecek
        public static String ADD_NEW_BASKET_PRODUCTS = link + basket_product;                  // get  --> basket_id si id olarak gönderilecek

    }
}
