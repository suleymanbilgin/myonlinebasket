package com.suleymanbilgin.myonlinebasket.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.suleymanbilgin.myonlinebasket.BasketDetailActivity;
import com.suleymanbilgin.myonlinebasket.MainActivity;
import com.suleymanbilgin.myonlinebasket.R;
import com.suleymanbilgin.myonlinebasket.models.Basket;
import com.suleymanbilgin.myonlinebasket.models.BasketProduct;
import com.suleymanbilgin.myonlinebasket.utils.AppValues;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by suleyman on 26.04.2016.
 */
public class BasketDetailAdapter extends RecyclerView.Adapter<BasketDetailAdapter.ViewHolder> {

    List<BasketProduct> basketProductList;
    Context mContext;
    BasketDetailActivity bdAct;
    RequestQueue mRequestQueue;
    int userId;

    public BasketDetailAdapter(List<BasketProduct> basketProductList, Context mContext, BasketDetailActivity bdAct, RequestQueue mRequestQueue, int userId) {
        this.basketProductList = basketProductList;
        this.mContext = mContext;
        this.bdAct = bdAct;
        this.mRequestQueue = mRequestQueue;
        this.userId = userId;
    }

    @Override
    public BasketDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_basket_product, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(BasketDetailAdapter.ViewHolder holder, int position) {
        final BasketProduct basketProductListItem = basketProductList.get(position);
        Log.e("PRODUCT", basketProductListItem.getProductName());
        holder.cbProductName.setText(basketProductListItem.getProductName());
        holder.tvProductCount.setText(basketProductListItem.getCount());
        if (basketProductListItem.getBought().equals("0")) {
            holder.cbProductName.setChecked(false);
        } else {
            holder.cbProductName.setChecked(true);
        }

        holder.cbProductName.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.e("checked", String.valueOf(basketProductListItem.getId() + " " + isChecked));
                String url;
                if (isChecked) {
                    url = AppValues.LINK.UPDATE_BASKET_PRODUCTS + "/" + String.valueOf(basketProductListItem.getId()) + ".json"
                            + "?user_id=" + String.valueOf(userId) + "&bought=1";
                } else {
                    url = AppValues.LINK.UPDATE_BASKET_PRODUCTS + "/" + String.valueOf(basketProductListItem.getId()) + ".json"
                            + "?user_id=" + String.valueOf(userId) + "&bought=0";
                }

                JsonObjectRequest json = new JsonObjectRequest(Request.Method.PATCH, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject basketProduct = response.getJSONObject("basketproduct");
                            JSONObject buyer = response.getJSONObject("buyer");

                        } catch (Exception e) {

                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
                mRequestQueue.add(json);
            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductCount;
        public CheckBox cbProductName;

        public ViewHolder(View itemView) {
            super(itemView);
            cbProductName = (CheckBox) itemView.findViewById(R.id.cb_product_name);
            tvProductCount = (TextView) itemView.findViewById(R.id.tv_product_count);
        }
    }

    @Override
    public int getItemCount() {
        return basketProductList == null ? 0 : basketProductList.size();
    }
}
