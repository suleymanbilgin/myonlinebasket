package com.suleymanbilgin.myonlinebasket.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.suleymanbilgin.myonlinebasket.BasketDetailActivity;
import com.suleymanbilgin.myonlinebasket.MainActivity;
import com.suleymanbilgin.myonlinebasket.R;
import com.suleymanbilgin.myonlinebasket.models.Basket;

import java.util.List;

/**
 * Created by suleyman on 25.04.2016.
 */
public class BasketListAdapter extends RecyclerView.Adapter<BasketListAdapter.ViewHolder> {

    List<Basket> basketsList;
    Context mContext;
    MainActivity mAct;

    public BasketListAdapter(List<Basket> basketsList, Context mContext, MainActivity mAct) {
        this.basketsList = basketsList;
        this.mContext = mContext;
        this.mAct = mAct;
    }

    @Override
    public BasketListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_basket, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(BasketListAdapter.ViewHolder holder, int position) {
        final Basket basketListItem = basketsList.get(position);
        holder.basketName.setText(basketListItem.getBasketname());
        Log.e("id", String.valueOf(basketListItem.getId()));
        holder.cvBasket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent basketProductListActivity = new Intent(mAct, BasketDetailActivity.class);
                basketProductListActivity.putExtra("basket_id", basketListItem.getId());
                mAct.startActivity(basketProductListActivity);
            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView basketName;
        public CardView cvBasket;

        public ViewHolder(View itemView) {
            super(itemView);
            basketName = (TextView) itemView.findViewById(R.id.tv_basket_name);
            cvBasket = (CardView) itemView.findViewById(R.id.cv_basket);
        }
    }

    @Override
    public int getItemCount() {
        return basketsList == null ? 0 : basketsList.size();
    }
}
