package com.suleymanbilgin.myonlinebasket;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.suleymanbilgin.myonlinebasket.custom.BaseActivity;
import com.suleymanbilgin.myonlinebasket.utils.AppValues;

import org.json.JSONObject;

/**
 * Created by suleyman on 27.04.2016.
 */
public class AddNewProductToBasketActivity extends BaseActivity {
    EditText etQuantity;
    EditText etProductName;
    LinearLayout llCountable;
    CheckBox cbCountable;
    Toolbar toolbar;
    int user_id;
    SharedPreferences sharedPref;
    int basket_id;
    String pcount = "0";
    String countable = "0";
    CardView cvNewProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_product_basket);

        init();
    }

    void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.add_new_item));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mRequestQueue = Volley.newRequestQueue(this);

        Bundle extras = getIntent().getExtras();
        basket_id = extras.getInt("basket_id");
        sharedPref = getSharedPreferences("com.suleymanbilgin.myonlinebasket.prefs", Context.MODE_PRIVATE);

        user_id = sharedPref.getInt(AppValues.USER.USER_ID, 0);

        cvNewProduct = (CardView) findViewById(R.id.cv_new_product);
        etQuantity = (EditText) findViewById(R.id.et_quantity);
        etProductName = (EditText) findViewById(R.id.et_product_name);
        llCountable = (LinearLayout) findViewById(R.id.ll_countable);
        cbCountable = (CheckBox) findViewById(R.id.cb_countable);

        cbCountable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    llCountable.setVisibility(View.VISIBLE);
                    pcount = etQuantity.getText().toString();
                    countable = "1";
                } else {
                    llCountable.setVisibility(View.GONE);
                    pcount = "0";
                    countable = "0";
                }
            }
        });

        etQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                pcount = etQuantity.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        cvNewProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewProduct();
            }
        });
    }

    void addNewProduct() {
        String url = AppValues.LINK.ADD_NEW_BASKET_PRODUCTS + ".json?"
                + "productname=" + etProductName.getText().toString()
                + "&countable=" + countable
                + "&count=" + pcount
                + "&user_id=" + String.valueOf(user_id)
                + "&bought=0"
                + "&mbasket_id=" + String.valueOf(basket_id);
        url = url.replaceAll(" ", "%20");

        Log.e("URL", url);

        JsonObjectRequest json = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.added), Snackbar.LENGTH_LONG)
                            .show();
                    finish();
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.added), Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        mRequestQueue.add(json);
    }


}
